package com.industry.hollywood.staff;

import com.industry.hollywood.staff.profile.EmployeeFunctionality;
import thirdparty.service.FinancialService;
import thirdparty.staff.StudioEmployee;

import java.util.Random;

import static thirdparty.Salaries.CAMERA_MAN;

public class CameraMan extends StudioEmployee implements EmployeeFunctionality {

    public CameraMan(String name) {
        super(name, CAMERA_MAN.proposedSalary);
    }

    @Override
    public void pay(StudioEmployee person, FinancialService financialService) {

    }

    @Override
    public boolean act() {
        return true;
    }

    @Override
    public boolean shoot() {
        // like an actor, cameraman with 4% probability may ruin the whole day
        return new Random().nextDouble() > 0.04;
    }

    @Override
    public StudioEmployee hire(String name, String personType) {
        return null;
    }

}
