package com.industry.hollywood;

import com.industry.hollywood.finance.FinanceDepartment;
import com.industry.hollywood.movie.Movie;
import com.industry.hollywood.movie.ProducingService;
import com.industry.hollywood.movie.ProducingService.MovieStatistics;
import com.industry.hollywood.staff.*;
import com.industry.hollywood.staff.team.StudioStaff;
import thirdparty.Genre;
import thirdparty.service.BudgetIsOverException;
import thirdparty.service.InsufficientBudgetException;
import thirdparty.staff.StudioEmployee;

import static thirdparty.Salaries.ACTOR;
import static thirdparty.Salaries.CAMERA_MAN;

public class MovieStudio {

    private static final Long INITIAL_BUDGET = 1000000L;
    private static final int POTENTIAL_RISK = 15; // percent

    private final FinanceDepartment financialService;
    private final StaffingService staffingService;
    private final ProducingService producingService;
    private final MovieStatistics movieStatistics;

    public MovieStudio() {
        this.financialService = new FinanceDepartment();

        this.staffingService = new StaffingService();

        this.producingService = new ProducingService();
        this.movieStatistics = this.producingService.loadMovieDatabase("film_archive.yaml");
    }

    public Movie createMovie(String recruiterName, String accountantName, MovieDefinition movieDefinition) throws InsufficientBudgetException {
        Movie movie = new Movie(movieDefinition.getMovieName(), movieDefinition.getMovieGenre(),
                movieDefinition.getMovieStaff());

        this.financialService.initBudget(INITIAL_BUDGET + movieDefinition.getBudget());

        this.staffingService.hireNewStaff(
                new Recruiter(recruiterName),
                new Accountant(accountantName));

        if (canBeProduced(this.financialService.getBudget(), movieDefinition.getProductionScheduleDaysCount(),
                staffingService, movieDefinition.getMovieStaff())) {

            this.staffingService.hireNewStaff(movieDefinition.getMovieStaff());
            this.producingService.initMovieProduction(movieDefinition.getProductionScheduleDaysCount());

            while (this.producingService.hasNextWorkingDay()) {
                // produce a movie
                if (this.producingService.lightsCameraAction(this.staffingService))
                    producingService.progress();
                movie.updateContent();
                // pay salary to every member of a team
                try {
                    this.financialService.paySalary(this.staffingService.getStaff());
                } catch (BudgetIsOverException e) {
                    System.out.printf("Movie production failed. Budget is over. Current progress is %.2f %n",
                            (1 - producingService.getProgress() * 1.0 / movieDefinition.getProductionScheduleDaysCount()) * 100);
                    return movie;
                }

            }

            movie.success();
            printProducedMovieStatistics(movieDefinition);
            producingService.addMovieToArchive(movie);
        } else {
            throw new InsufficientBudgetException("Movie cannot be produced - budget is insufficient");
        }

        return movie;
    }

    private void printProducedMovieStatistics(MovieDefinition movieDefinition) {
        Long budgetSpent = movieDefinition.getBudget() + INITIAL_BUDGET - this.financialService.getBudget();
        System.out.printf("Budget: %d initial, %d spent, %d economy\n",
                movieDefinition.getBudget() + INITIAL_BUDGET, budgetSpent, this.financialService.getBudget());

        this.staffingService.getStaff().forEach(person -> {
            if (person instanceof Actor) {
                System.out.printf("Actor: '%s', earned money: %d, salary: %d\n", person.getName(),
                        person.getEarnedMoney(), person.getSalary());
                return;
            }
            if (person instanceof CameraMan) {
                System.out.printf("Cameraman: '%s', earned money: %d, salary: %d\n", person.getName(),
                        person.getEarnedMoney(), person.getSalary());
                return;
            }
            if (person instanceof Accountant) {
                System.out.printf("Accountant: '%s', earned money: %d, salary: %d\n", person.getName(),
                        person.getEarnedMoney(), person.getSalary());
                return;
            }
            System.out.printf("Recruiter: '%s', earned money: %d, salary: %d\n", person.getName(),
                    person.getEarnedMoney(), person.getSalary());
        });
    }

    public void printMovieArchiveStatistics() {
        if (!movieStatistics.isEmpty()) {
            System.out.printf("Total: %d actors, %d cameramen, superstars: [%s]\n",
                    movieStatistics.getTotalActors(),
                    movieStatistics.getTotalCameramen(),
                    String.join(", ", movieStatistics.getSuperstars())
            );
        }
    }

    private boolean canBeProduced(Long proposedBudget, int daysInProduction, StaffingService staffingService, StudioStaff movieStaff) {
        Long estimatedBudgetUnhired = movieStaff.getActorsCollection().size() * ACTOR.proposedSalary +
                movieStaff.getCameramanCollection().size() * CAMERA_MAN.proposedSalary;
        Long estimatedBudgetHired = staffingService.getStaff().stream()
                .map(StudioEmployee::getSalary)
                .reduce(0L, Long::sum) * daysInProduction * Math.round(100.0 + POTENTIAL_RISK / 100.0);
        return proposedBudget >= (estimatedBudgetUnhired + estimatedBudgetHired);
    }

    public static class MovieDefinition {
        private final Long budget;
        private final String movieName;
        private final Genre movieGenre;
        private final StudioStaff movieStudioStaff;
        private final int daysInProduction;

        public Long getBudget() {
            return budget;
        }

        public String getMovieName() {
            return movieName;
        }

        public MovieDefinition(Long budget, String movieName, Genre movieGenre,
                               StudioStaff movieStudioStaff, int daysInProduction) {
            this.budget = budget;
            this.movieName = movieName;
            this.movieGenre = movieGenre;
            this.movieStudioStaff = movieStudioStaff;
            this.daysInProduction = daysInProduction;
        }

        public Genre getMovieGenre() {
            return movieGenre;
        }

        public StudioStaff getMovieStaff() {
            return movieStudioStaff;
        }

        public int getProductionScheduleDaysCount() {
            return daysInProduction;
        }
    }

}
