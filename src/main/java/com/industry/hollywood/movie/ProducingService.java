package com.industry.hollywood.movie;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.industry.hollywood.staff.Actor;
import com.industry.hollywood.staff.CameraMan;
import com.industry.hollywood.staff.StaffingService;
import com.industry.hollywood.staff.profile.EmployeeFunctionality;
import thirdparty.staff.StudioEmployee;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class ProducingService {

    private List<Movie> movieArchive;
    private MovieProductionSchedule productionSchedule;

    public ProducingService() {
        this.movieArchive = new ArrayList<>();
    }

    public MovieStatistics loadMovieDatabase(String fileName) {
        movieArchive = new ArrayList<>();
        final URL resource = getClass().getClassLoader().getResource(fileName);
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        try {
            Movie[] movies = om.readValue(resource, Movie[].class);
            for (Movie movie : movies) {
                addMovieToArchive(movie);
            }
            return getArchiveStatistics();
        } catch (IOException e) {
            System.out.println("Movie archive is damaged or empty");
            return new MovieStatistics(new ArrayList<>());
        }
    }

    private MovieStatistics getArchiveStatistics() {
        MovieStatistics movieStatistics = new MovieStatistics(movieArchive);

        System.out.printf("Movies in archive: %d\n", movieArchive.size());
        movieArchive.stream()
                .peek(movie -> {
                    String currentMovieGenre = movie.getGenre().name();
                    movieStatistics.getGenres().put(
                            currentMovieGenre,
                            movieStatistics.getGenres().getOrDefault(currentMovieGenre, 1)
                    );
                })
                .forEach(movie -> {
                    movieStatistics.incActorsCount(movie.getCrew().get("Actor"));
                    movieStatistics.incCameramenCount(movie.getCrew().get("Cameraman"));
                    movieStatistics.addSuperStars(movie.getSuperstars());
                });
        return movieStatistics;
    }

    public void addMovieToArchive(Movie movie) {
        movieArchive.add(movie);
    }

    public void initMovieProduction(int daysInProduction) {
        this.productionSchedule = new MovieProductionSchedule(daysInProduction);
    }

    public boolean hasNextWorkingDay() {
        return productionSchedule.getDaysSpentOnProduction() > 0;
    }

    public void progress() {
        productionSchedule.setDaysInProduction(productionSchedule.getDaysSpentOnProduction() - 1);
    }

    // returns true if producing day is a success
    public boolean lightsCameraAction(StaffingService staffingService) {
        final List<StudioEmployee> staff = staffingService.getStaff();
        return staff.stream()
                .filter(person -> person instanceof Actor || person instanceof CameraMan)
                .map(person -> ((EmployeeFunctionality) person).act() && ((EmployeeFunctionality) person).shoot())
                .reduce(true, (crewSuccess, crewAction) -> crewSuccess && crewAction);
    }

    public int getProgress() {
        return productionSchedule.getDaysSpentOnProduction();
    }

    public static class MovieStatistics {
        Map<String, Integer> movieGenres;
        int totalActors;
        int totalCameramen;
        Set<String> superstars;

        public MovieStatistics(List<Movie> movieArchive) {
            this.movieGenres = new HashMap<>();
            totalActors = 0;
            totalCameramen = 0;
            this.superstars = new HashSet<>();
        }

        public boolean isEmpty() {
            return totalActors + totalCameramen + superstars.size() > 0;
        }

        public Map<String, Integer> getGenres() {
            return movieGenres;
        }

        public Set<String> getSuperstars() {
            return superstars;
        }

        public void incActorsCount(Integer actorsCount) {
            totalActors += actorsCount;
        }

        public void incCameramenCount(Integer cameramenCount) {
            totalCameramen += cameramenCount;
        }

        public int getTotalActors() {
            return totalActors;
        }

        public int getTotalCameramen() {
            return totalCameramen;
        }

        public void addSuperStars(List<String> superstars) {
            this.superstars.addAll(superstars);
        }
    }
}
