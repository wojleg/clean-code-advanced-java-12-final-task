package com.industry.hollywood.movie;

import com.industry.hollywood.staff.Actor;
import com.industry.hollywood.staff.team.StudioStaff;
import thirdparty.Genre;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Movie {

    private String name;
    private Genre genre;
    private boolean isFinished;
    private int daysInProduction;
    private Map<String, Integer> crew;
    private List<String> superstars;

    public Movie() {
    }

    public Movie(String name, Genre genre, StudioStaff staff) {
        this.name = name;
        this.genre = genre;
        this.isFinished = false;
        this.daysInProduction = 0;
        this.crew = new HashMap<>();
        this.superstars = new ArrayList<>();
        this.setCrewFromStaffCollection(staff);
    }

    public void setCrewFromStaffCollection(StudioStaff crew) {
        this.crew.put("Actor", crew.getActorsCollection().size());
        this.crew.put("Cameraman", crew.getCameramanCollection().size());
        for (Actor actor : crew.getActorsCollection()) {
            if (actor.isSuperStar()) {
                superstars.add(actor.getName());
            }
        }
    }

    public Map<String, Integer> getCrew() {
        return crew;
    }

    public List<String> getSuperstars() {
        return superstars;
    }

    public void success() {
        this.isFinished = true;
    }

    public void updateContent() {
        this.daysInProduction++;
    }

    public String getName() {
        return name;
    }

    public Genre getGenre() {
        return genre;
    }

    @Override
    public String toString() {
        return String.format("Movie `%s` [%s], status: %s, days in production: %d",
                name, genre, isFinished ? "finished" : "in production", daysInProduction);
    }

    public boolean isFinished() {
        return isFinished;
    }
}
